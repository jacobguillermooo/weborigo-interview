const router = require("express").Router();
const WordPair = require("../models/wordPair");

const shuffle = (array) => {
  let currentIndex = array.length;
  let randomIndex;
  // While there remain elements to shuffle.
  while (currentIndex != 0) {
    // Pick a remaining element.
    randomIndex = Math.floor(Math.random() * currentIndex);
    currentIndex--;

    // And swap it with the current element.
    [array[currentIndex], array[randomIndex]] = [
      array[randomIndex],
      array[currentIndex],
    ];
  }

  return array;
};

//create a new word pair
router.post("/create", async (req, res) => {
  const { english, serbian, picture } = req.body;
  try {
    const newWordPair = new WordPair({
      english,
      serbian,
      picture,
    });
    await newWordPair.save();
    res.status(201).json(newWordPair);
  } catch (error) {
    res.status(400).json(error.message);
  }
});

//get all wordpairs
router.get("/all", async (req, res) => {
  try {
    const allWordPairs = await WordPair.find();
    res.status(200).json(shuffle(allWordPairs));
  } catch (error) {
    res.status(500).json(error.message);
  }
});

module.exports = router;
