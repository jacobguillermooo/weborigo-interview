const mongoose = require("mongoose");

const wordPairSchema = mongoose.Schema(
  {
    english: {
      type: String,
      required: true,
    },
    serbian: {
      type: String,
      required: true,
    },
    picture: {
      type: String,
      required: true,
    },
  },
  {
    timestamps: true,
  }
);

module.exports = mongoose.model("WordPair", wordPairSchema);
