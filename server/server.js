const express = require("express");
const mongoose = require("mongoose").set("strictQuery", false);
const cors = require("cors");
require("dotenv").config();

const PORT = process.env.PORT;

const app = express();
app.use(express.json());
app.use(cors());

mongoose.connect(
  `mongodb+srv://jakiSundays:${process.env.MONGODB_PASSWORD}@learning-tool.xenvwsa.mongodb.net/?retryWrites=true&w=majority`,
  () => console.log("Connected to DB")
);

const wordPairRoutes = require("./routes/wordPairRoute");
app.use("/wordpair", wordPairRoutes);


app.listen(PORT, () =>
  console.log(`server running on http://localhost:${PORT}/`)
);
