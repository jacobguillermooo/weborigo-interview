# Setup guide

## Clone repository

### Open new terminal and type:
    cd server
    npm install
    npm run serverStart

### Open new terminal and type:
    cd client
    npm install
    npm run dev

### Postman/REST Client
    I usually use REST Client in vscode.
    Requests at server/try.rest

## Datatable
    Front-end: 
        React.js
        html/css
    
    Back-end: 
        Node.js
        express
        mongoose




