import "./App.css";
import logo from "./assets/logo.png";
import LearningTool from "./components/LearningTool.jsx";

function App() {
  return (
    <section className="app-container">
      <img src={logo} />
      <LearningTool />
    </section>
  );
}

export default App;
