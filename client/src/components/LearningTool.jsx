import React, { useState, useEffect } from "react";
import "../styles/LearningTool.css";
import { HiOutlineThumbUp, HiOutlineThumbDown } from "react-icons/hi";
import axios from "axios";

const LearningTool = () => {
  const [text, setText] = useState("");
  const [questions, setQuestions] = useState([]);
  const [current, setCurrent] = useState(0);
  const [correct, setCorrect] = useState(0);
  const [incorrect, setIncorrect] = useState(0);
  const [answer, setAnswer] = useState();
  const [clue, setClue] = useState();

  useEffect(() => {
    fetchData();
  }, []);

  useEffect(() => {
    checkAnswer();
    if (questions.length > 0 && current === questions.length) {
      window.location.reload();
    }
  }, [current, questions]);

  const fetchData = async () => {
    try {
      const response = await axios("http://localhost:8000/wordpair/all");
      setQuestions(response.data);
    } catch (error) {
      console.error(error);
    }
  };

  const onEnter = (e) => {
    if (e.keyCode === 13) {
      updateScore();
    }
  };

  const updateScore = () => {
    if (answer === text.toLocaleLowerCase()) {
      setCorrect(correct + 1);
    } else {
      setIncorrect(incorrect + 1);
    }
    setCurrent(current + 1);
    setText("");
  };

  const checkAnswer = () => {
    const english = questions[current]?.english;
    const serbian = questions[current]?.serbian;
    const words = [english, serbian];
    const randomIndex = Math.floor(Math.random() * words.length);
    const neededAnswer = words[randomIndex];
    const question = words.filter((word) => word !== neededAnswer)[0];
    setAnswer(neededAnswer);
    setClue(question);
  };

  return (
    <div className="learning-container">
      <div className="form">
        <img src={questions[current]?.picture} />
        <h4> {clue} </h4>
        <input
          type="text"
          placeholder="Enter word..."
          value={text}
          onChange={(e) => setText(e.target.value)}
          onKeyDown={(e) => onEnter(e)}
        />
        <button onClick={() => updateScore()}>Let's see</button>
      </div>
      <div className="results">
        <div
          className="result-wrapper"
          onClick={() => console.log(questions[current])}
        >
          <HiOutlineThumbUp />
          {`${correct} / ${questions.length}`}
        </div>
        <div className="result-wrapper" onClick={() => console.log(questions)}>
          <HiOutlineThumbDown />
          {`${incorrect} / ${questions.length}`}
        </div>
      </div>
    </div>
  );
};

export default LearningTool;
